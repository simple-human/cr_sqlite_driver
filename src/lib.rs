//! # cr_sqlite_driver
//! 
//! A data source module for [control\_room](https://gitlab.com/simple-human/control_room) library.
//! It allows fetching data from sqlite database.
//! 
//! ```rust
//! use control_room::{DriverRegister, SourcesConfig, Observables};
//! use cr_sqlite_driver::SqliteDriver;
//! use serde_json::json;
//! 
//! let drivers: DriverRegister = Default::default();
//! drivers.register::<SqliteDriver>("sqlite_source".into());
//! 
//! let sources_config: SourcesConfig = serde_json::from_value(json!(
//!         {"sqlite": {
//!            "driver": "sqlite_source",
//!            "options": {
//!                "file": "path_to_db_file.db"
//!            }
//!         }}
//!      ))
//!      .unwrap();
//! 
//!      let observables: Observables = serde_json::from_value(json!(
//!              {
//!                  "date": {
//!                  "datasets": [{
//!                      "source": "sqlite_source",
//!                      "view_options": {
//!                          "name": "db test"
//!                      },
//!                      "data_options": {
//!                          "interval": "30 s",
//!                          "query": "select y from data where x=:x",
//!                          "collect": false,
//!                          "shape": "Value"
//!                      }
//!                  }],
//!                  "client": {
//!                      "controls": {
//!                          "x": 1
//!                      }
//!                  }
//!              }
//!          }
//!      ))
//!      .unwrap();
//! ```
//! 
//! The source option *file* is a path to an sqlite database file.
//! 
//! Observable dataset options are
//! - *interval* - optional command execution period in [humantime](https://docs.rs/humantime) format; if omitted the query is executed once;
//! - *query* - an sql query.
//! - *collect* - a boolean, if false reduce the resulting raws to the first raw;
//! - *shape* - can be "Object" or "Value". In the first case the result raws are presented as a JSON object with columns names as keys, 
//!   else the resulting columns are reduced to the first data column as JSON array.
//!
//! Combinations of *collect* and *shape* are used to flatten table structure to an array or single value JSON.
//!
//! For example
//!
//! ```json
//! "query": "select * from table"
//! "collect": true                             =>     [{ "id": 1, "x": 2, "y": 4}, { "id": 2, "x": 3, "y": 9}]
//! "shape": "Object"
//!
//! "query": "select y from table"
//! "collect": true                             =>     [4,9]
//! "shape": "Value"
//!
//! "query": "select y from table where id=2"
//! "collect": false                            =>     9
//! "shape": "Value"
//! ```
//! 
//! The client side *controls* parameter is an JSON object with query parameters.
//!
use control_room::{Driver, Configurator, Error};
use serde_json::Value;
use serde::Deserialize;
use std::sync::{Mutex, Arc};
use std::collections::HashMap;
use futures::stream::BoxStream;
use futures::stream::StreamExt;
use tokio::time;

use rusqlite::Connection;
use serde_rusqlite::{from_row, from_row_with_columns};

pub struct SqliteDriver {
    options: ConfigOptions,
    connection: Arc<Mutex<Connection>>,
}

#[derive(Debug, Deserialize, Clone)]
struct ConfigOptions {
    file: String
}

#[derive(Debug, Deserialize, Clone)]
struct ObservableOptions {
    query: String,
#[serde(default="default_collect")]
    collect: bool,
#[serde(default="default_shape")]
    shape: Shape,
    interval: Option<String>,
}

fn default_collect() -> bool { true }

#[derive(Debug, Deserialize, Clone)]
enum Shape {
    Value,
    Object
}

fn default_shape() -> Shape { Shape::Object }


impl Driver for SqliteDriver {

    fn observable(
        &self,
        options: Arc<Value>,
        controls: Arc<Value>,
    ) -> Result<BoxStream<'static, Result<Value, Error>>, Error> {

        let options: ObservableOptions = serde_json::from_value((*options).clone())
            .map_err(|e| {
                Error::DriverError(format!("Bad options for SqliteDriver: {}\n cmd: {}, Options: {:?}", e, &self.options.file, &options))
            })?;
        let options = Arc::new(options);

        let connection = self.connection.clone();

        let stream = if let Some(input) = &options.interval {
            let parsed = input
                .parse::<humantime::Duration>()
                .map_err(|e| Error::DriverError(format!("can't parse interval: {}", e)))?;

            time::interval(parsed.into())
                .map(move |_| exec(options.clone(), connection.clone(), controls.clone()))
                .boxed()
        } else {
            futures::stream::once(async move { exec(options, connection, controls) }).boxed()
        };

        Ok(stream)

    }
}

fn exec(options: Arc<ObservableOptions>, connection: Arc<Mutex<Connection>>, controls: Arc<Value>) -> Result<Value, Error> {
        let connection = connection.lock()
            .map_err(|e| {
                Error::DriverError(format!("Mutex problem in SqliteDriver: {}", e))
            })?;
        let mut stmt = connection.prepare(&options.query)
            .map_err(|e| {
                Error::DriverError(format!("Bad query in SqliteDriver: {}\nOptions: {:?}", e, options))
            })?;
        let params = serde_rusqlite::to_params_named(&(*controls))
            .map_err(|e| {
                Error::DriverError(format!("Bad controls in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
            })?;

        match options.shape {
            Shape::Value => {

                let mut res= stmt.query_and_then_named(&params.to_slice(), |row| from_row::<Value>(row))
                   .map_err(|e| {
                       Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                   })?;

                if options.collect {
                    let res = &res.collect::<Result<Vec<_>,_>>()
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })?;
                   serde_json::to_value(res)
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })
                } else {
                   let res = &res.next();
                   if let Some(res) = res {
                   serde_json::to_value(res.as_ref()
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })?)
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })
                   } else {
                       Err(Error::DriverError(format!("Bad result in SqliteDriver: empty result \nOptions: {:?}\nControls: {:?}", options, controls)))
                   }
                }

            },
            Shape::Object => {
                let columns = serde_rusqlite::columns_from_statement(&stmt);
                let mut res = stmt.query_and_then_named(&params.to_slice(), |row| from_row_with_columns::<HashMap<String, Value>>(row, &columns))
                   .map_err(|e| {
                       Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                   })?;

                if options.collect {
                    let res = &res.collect::<Result<Vec<_>,_>>()
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })?;
                   serde_json::to_value(res)
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })
                } else {
                   let res = &res.next();
                   if let Some(res) = res {
                   serde_json::to_value(res.as_ref()
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })?)
                       .map_err(|e| {
                           Error::DriverError(format!("Bad result in SqliteDriver: {}\nOptions: {:?}\nControls: {:?}", e, options, controls))
                       })
                   } else {
                       Err(Error::DriverError(format!("Bad result in SqliteDriver: empty result \nOptions: {:?}\nControls: {:?}", options, controls)))
                   }
                }
            }
        }
}

impl Configurator for SqliteDriver {
    fn configure(options: Value) -> Result<Box<dyn Driver>, Error> {
        let options: ConfigOptions = serde_json::from_value(options).map_err(|e| {
            Error::DriverError(format!("{}", e))
        })?;
        let connection = Connection::open(&options.file).map_err(|e| {
            
            Error::DriverError(format!("Cant open DB: {}", e))
        })?;
        let connection = Arc::new(Mutex::new(connection));

        Ok(
            Box::new(SqliteDriver {
                options: options.clone(),
                connection: connection.clone()
        }))
    }
}


#[cfg(test)]
#[allow(unused_must_use)]
mod test {
    use super::*;
    use control_room::Configurator;
    use serde_json::json;
    use std::sync::Arc;
    use actix::prelude::*;
    use std::thread;
    use std::sync::mpsc::channel;
    use mktemp::Temp;
    use rusqlite::{Connection, params};
    
    #[test]
    fn driver_test() -> Result<(), String> {

        let (sender, receiver) = channel();

        thread::spawn( move || {
            System::run(move || {
                actix::Arbiter::spawn(async move {
                    sender.send(helper().await).expect("can't send msg");
                });
            });
        });

        receiver.recv().expect("can't receive msg")
    }
    
    async fn helper() -> Result<(), String> {
        let db = Temp::new_path();
        let con = Connection::open(db.as_ref()).map_err(|e| format!("can't open tmp db file: {}",e))?;
        con.execute("
                CREATE TABLE data (id INTEGER PRIMARY KEY, x REAL, y REAL)
              ", params![])
            .map_err(|e| format!("cant create table: {}",e))?;

        con.execute("
                INSERT INTO data (x, y) VALUES (2, 4)
              ", params![])
            .map_err(|e| format!("cant insert into table: {}",e))?;
        con.execute("
                INSERT INTO data (x, y) VALUES (3, 9)
              ", params![])
            .map_err(|e| format!("cant insert into table: {}",e))?;
        con.close()
            .map_err(|_| format!("error on closing db"))?;

        let configure_options = json! (
            {
                "file": db.as_ref()
            }
        ); 

        let driver = <SqliteDriver as Configurator>::configure(configure_options)
            .map_err(|e| format!("Can't create Driver: {}", e))?;

        let observable_options = json!({
            "query": "select * from data" ,
            "collect": true,
            "shape": "Object"
        });

        let expected = json!(
            [
                { "id": 1, "x": 2.0, "y": 4.0},
                { "id": 2, "x": 3.0, "y": 9.0},
            ]
        );

        let mut stream = driver.observable(Arc::new(observable_options), Arc::new(json!({})))
            .map_err(|e| format!("Can't create observable: {}", e))?;
        let result = stream.next().await
            .ok_or(format!("empty stream"))?
            .map_err(|e| format!("Can't fetch data: {}", e))?;
        if result != expected { 
           return Err(format!("1: Wrong answer: got {}, expected {}", result, expected))
        }



        let observable_options = json!({
            "query": "select x from data" ,
            "collect": true,
            "shape": "Value"
        });

        let expected = json!(
            [2.0,3.0]
        );

        let mut stream = driver.observable(Arc::new(observable_options), Arc::new(json!({})))
            .map_err(|e| format!("Can't create observable: {}", e))?;
        let result = stream.next().await
            .ok_or(format!("empty stream"))?
            .map_err(|e| format!("Can't fetch data: {}", e))?;
        if result != expected { 
           return Err(format!("2: Wrong answer: got {}, expected {}", result, expected))
        }



        let observable_options = json!({
            "query": "select x,y from data where id=1",
            "collect": false,
            "shape": "Object"
        });

        let expected = json!(
            { "x": 2.0, "y": 4.0 }
        );

        let mut stream = driver.observable(Arc::new(observable_options), Arc::new(json!({})))
            .map_err(|e| format!("Can't create observable: {}", e))?;
        let result = stream.next().await
            .ok_or(format!("empty stream"))?
            .map_err(|e| format!("Can't fetch data: {}", e))?;
        if result != expected { 
           return Err(format!("3: Wrong answer: got {}, expected {}", result, expected))
        }



        let observable_options = json!({
            "query": "select x from data where id=1",
            "collect": false,
            "shape": "Value"
        });

        let expected = json!(
            2.0
        );

        let mut stream = driver.observable(Arc::new(observable_options), Arc::new(json!({})))
            .map_err(|e| format!("Can't create observable: {}", e))?;
        let result = stream.next().await
            .ok_or(format!("empty stream"))?
            .map_err(|e| format!("Can't fetch data: {}", e))?;
        if result != expected { 
           return Err(format!("4: Wrong answer: got {}, expected {}", result, expected))
        }

        let observable_options = json!({
            "query": "select y from data where id=:id",
            "collect": false,
            "shape": "Value"
        });

        let observable_controls = json!({
            "id": 2 
        });

        let expected = json!(
            9.0
        );

        let mut stream = driver.observable(Arc::new(observable_options), Arc::new(json!(observable_controls)))
            .map_err(|e| format!("Can't create observable: {}", e))?;
        let result = stream.next().await
            .ok_or(format!("empty stream"))?
            .map_err(|e| format!("Can't fetch data: {}", e))?;
        if result != expected { 
           return Err(format!("5: Wrong answer: got {}, expected {}", result, expected))
        }

        Ok(())
    }
}

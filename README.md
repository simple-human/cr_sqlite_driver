# cr_sqlite_driver

A data source module for [control\_room](https://gitlab.com/simple-human/control_room) library.
It allows fetching data from sqlite database.

```rust
use control_room::{DriverRegister, SourcesConfig, Observables};
use cr_sqlite_driver::SqliteDriver;
use serde_json::json;

let drivers: DriverRegister = Default::default();
drivers.register::<SqliteDriver>("sqlite_source".into());

let sources_config: SourcesConfig = serde_json::from_value(json!(
        {"sqlite": {
           "driver": "sqlite_source",
           "options": {
               "file": "path_to_db_file.db"
           }
        }}
     ))
     .unwrap();

     let observables: Observables = serde_json::from_value(json!(
             {
                 "date": {
                 "datasets": [{
                     "source": "sqlite_source",
                     "view_options": {
                         "name": "db test"
                     },
                     "data_options": {
                         "interval": "30 s",
                         "query": "select y from data where x=:x",
                         "collect": false,
                         "shape": "Value"
                     }
                 }],
                 "client": {
                     "controls": {
                         "x": 1
                     }
                 }
             }
         }
     ))
     .unwrap();
```

The source option *file* is a path to an sqlite database file.

Observable dataset options are
- *interval* - optional command execution period in [humantime](https://docs.rs/humantime) format; if omitted the command is executed once;
- *query* - an sql query.
- *collect* - a boolean, if false reduce the resulting raws to the first raw;
- *shape* - can be "Object" or "Value". In the first case the result raws are presented as a JSON object with columns names as keys, 
  else the resulting columns are reduced to the first data column as JSON array.

Combinations of *collect* and *shape* are used to flatten table structure to an array or single value JSON.

For example

```
 "query": "select * from table"
 "collect": true                             =>     [{ "id": 1, "x": 2, "y": 4}, { "id": 2, "x": 3, "y": 9}]
 "shape": "Object"
 
 "query": "select y from table"
 "collect": true                             =>     [4,9]
 "shape": "Value"
 
 "query": "select y from table where id=2"
 "collect": false                            =>     9
 "shape": "Value"
```


The client side *controls* parameter is an JSON object with query parameters.

